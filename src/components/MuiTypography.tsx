 
import { Typography } from "@mui/material"

export const MuiTypography = () => {
  return (
    <div>
        <Typography variant="h1">h1 heading</Typography>
        <Typography variant="h2">h2 heading</Typography>
        <Typography variant="h3">h3 heading</Typography>
        <Typography variant="h4">h4 heading</Typography>
        <Typography variant="h5">h5 heading</Typography>
        <Typography variant="h6">h6 heading</Typography>

        < Typography variant="subtitle1">sub title 1</Typography>
        <Typography variant="subtitle2">subtitle2</Typography>

        <Typography variant="body1">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iure vitae quas odio ad non odit enim neque iusto soluta beatae ab laboriosam sed quo suscipit delectus libero ipsum, sequi doloremque.</Typography>
        <Typography variant="body2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, natus accusantium? Repudiandae nulla nesciunt deleniti, ipsa aliquid illo? Rerum cum eveniet excepturi debitis eius impedit nisi neque eum iusto fugiat!</Typography>
    </div>
  )
}
