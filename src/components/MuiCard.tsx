import React from 'react'
import {Box,Card,CardContent,Typography,CardActions,Button,CardMedia} from "@mui/material"

export const MuiCard = () => {
  return (
    <Box width="300px">
        <Card>
            <CardMedia
             component='img'
             height='140'
             image='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHUAzwMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABgcDBAUCAf/EADsQAAEDAwEDCAgFAwUAAAAAAAABAgMEBREGEiExExZBUVNhcZMHFCIykaGx0SNSgbLBQpLCFWJygqL/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAQMEBQL/xAAqEQEAAgIABQQBBAMBAAAAAAAAAQIDEQQTFCExEjJBUXEiM2GRI0JDNP/aAAwDAQACEQMRAD8A7ZscYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADxyse1s8ozPVtIRuHr0z9Pab+BLy+Lu4hL5tsTi5vxBqUR1NraO2zupLZHHUTsXEkjlyxi9W7ipTky6nUNmHhPXHqv2ZdP63o7i9tPXtSkqVX2VV34b16kXoXuUUzRbtLzm4S1O9e8JWXMgAAAAAAAAAAAAAAAAAAAGjeLpTWehfV1jlRjVw1rd7nu6ETvPNrRWNysx45yW9MKtvWqrpdnuRZnU0C8IYXKiY71Tev07jJbLazrY+Hpj+Ny4f9W0nvfm6TxtdplSpqGp7NRMnhIqfyNyj0x9Dqmod71RMvjIqjcmo+mNyq7312v8AlvI2nUPiYRMIgBUyBIbDq242udqSzSVVJnDopXbSon+1V3ovcW0y2rP8M+XhqZI8ala1NURVdNFUU7kfFK1HscnSimuJ33ci0TWdSykoAAAAAAAAAAAAAAAAHiaWOCJ8072xxMRXPe5cI1E4qN6TETM6hUmsb8l8uaOgV3qkCbMKO3Z63Y7/AKIYsl/VPZ1+Hw8uvfy4JW0AAAAAAAAFk+jKvdNbKihkXK0z9pnXsu+yopqwW3GnM42mrRb7TMvYgAAAAAAAAAAAAAAABGNXwy3Wst1jimdFHUOfLO5qZXYYm5Pj88FWTvPpa+HmKRbJPwrW7UTrbc6qie/bWCRWbWMbSdC48DJaNTp06X9dYt9tQh6AAAAAAzU1LPVVjKOCNzqmR2y2Jdyqv6kxEzOnmbViN7SHROnaDUN5qbRc7hLb610apR+witdMnFrs/RMKvWTr4kme24dbSFvrtO6yrbNc4Vhn5Bcp0PwqKjmr0tVFXeW4e1tMnGatj2n5qcwAAAAAAAAAAAAAAAAR5mZtfPzvbT21ETuc5/2K/wDo0+OH/MoNr9I01RUcm3ZXk2bfe7H2wZ83ubuE3yoR0qaQAAAAfH+47wCdL1oVbJR00qtRXLE1Ud08EOhHiHBvM7mHJvWmKK5VL61EdFWci5rXxrjD8ew/dvyi9P2PFscT3W4+ItSIr8PGk9fx3quoLZqqzsrK+Fr44rjG5GytRGqqovfuxuXfxwZ6bm2m/NERj3PhKKz1RXotDy+wvFJtnKfA1xv5cq3p/wBWAl5AAAAAAAAAAAAAAAI9aGvk1dfqhUXYY2GFHd6NyqfT4ldffLRk7YaVQDWz9vVVwz0OY1P7GmbL75dHhf2YcMrXskME064ghkkXqYxXfQnUkzEeZdCm07eqnHI2ypVF35c3Y/dg9RjtPwqtnx18ykNp9H1VLh92qGwNxnkocPcvi7gn6ZLK4J+We/GV8VQlq5ai9Kp0FDaO4KBeFlz/AKPQ54+rx/tQ318Q4eX3z+W71eJ6Vqp0kmddR900/wC15jx/uOvn/wDP/S1jY5AAAAAAAAAAAAAAAAA8RRRxbXJsRu25XOwnvKvSoiEzMz5UxqeZJtQ3OX+n1h6Z8Fx/Bhv3tLt4Y1jrDzebRVWeSBlY3DpoUlRPy9bV70ItWa+THlrkiZr8LB9G1K2CwOqET26iZyqvc32UT5L8TTgj9O3P4y28mvpKy5jF4KJTCjbfGkl1pYsZa6pY3HWm0hgiP1ad286pM/w9XuD1e710GMIyokRPDaXHywLxqZRjtukSuGwvSSx296cHU0a/+UNtPbDjZf3LflvpxPStV2jmLz8en5H1Gfmn8mXHH+R1uIn/AAf0tE1OSAAAAAAAAfAAAAAAAAMdVO2mppqh3uxRukXwRMkTOo29VjdohTmn2rXalouXRFWaqSR6ccrlXGKne7tZf04p18QlHpTX8S2r04k/xLs/mGTgfFnW9HFXHPp5KdFTlaeRzXpnrXKL88fop6wTuulPGV1k39pSXMj492zG93U1VCY8wpPTqbd6tqLvVZ4/qhhp7odvN+3P4bmso+T1RcUVPekR3xahOX3S88PO8VVk6Nl5bS9td1Q7H9qqn8GrH7YcziI1ls7J7UIFb6VLb6TJI8exVMkkj/7JtL80cURGsroXtzOF39J4XueAAAAAAAAVVz7v3a0/kIZOdZ1+jxHPu/drT+Qg51jo8Rz7v3a0/kIOdY6PEc+792tP5CDnWOjxHPu/drT+Qg51kdHiOfd+7Wn8hBzrJ6PEc+792tP5CDnWOjxMNXrK9VlJNSzywclNG6N+zCiLsqmF3+BE5rTGk14XHWdw41FVS0VXFVU6oksLtpiqmURSuJ1O19qxaNS3Lzfq+9cj6++N3JZ2NiNG8eP0PV7zfy8Y8NMftYrRdqyz1K1FBIjHubsuRzdprk70IreazuE5Mdckas7HPu/drT+QhZzrKOjxPjtc31zVastPhUwv4CEc6yY4TFHdH6Od9HUQ1ECokkLkcxXJnenDJXEzE7aJrEx6ZZbncKi6Vr6yrVizPREdsN2U3JjgTafVO5RSkUj0w6Ns1XdrXRMo6SSFIWZwjokcu9c8T1XLasahVfhsd7eqW3z7v3a0/kIeudZ56PE0anUtyqrnTXGV8PrVMipG5saImFzxTp4qeZyWmYl7rgpWs1+Jb3Pu/drT+Qh651njo8Rz7v3a0/kIOdY6PEc+792tP5CDnWOjxHPu/drT+Qg51jo8Rz7v3a0/kIOdZHR4jn3fu1p/IQc6yejxHPu/drT+Qg51jo8Rz7v3a0/kIOdY6PEjJS0gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//9k='
             alt="unsplash image"
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component='div'>
                    React
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore, porro laudantium reprehenderit modi dolores, voluptas similique ad est, debitis doloremque possimus! Asperiores nesciunt rem dolores ea facilis exercitationem tenetur consequatur?
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">share</Button>
                <Button size="small">Learn more</Button>
            </CardActions>
        </Card>
    </Box>
  )
}
